# Native Cloud Native Buildpacks in Gitlab CI/CD

**Warning:** This project is old and unmaintained, but the idea is sound and can probably be expanded to build a DinD-free CNB platform for GitLab CI/CD.

This is an experiment that builds container images natively using Cloud Native Buildpacks builder images, instead of relying on Pack and Docker-in-Docker (which [Auto Build](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Build.gitlab-ci.yml) currently does).
